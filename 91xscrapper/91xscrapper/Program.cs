﻿using System;
using scrapper91x.Model;
using HtmlAgilityPack;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace scrapper91x
{
    class Program
    {
        static void Main(string[] args)
        {
            var lines = new List<string>();
            lines = GetLines();
            lines = CleanLines(lines);
            PrintList(lines);
            lines = ExtractInfo(lines);
            PrintList(lines);
            Console.ReadKey();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lines"></param>
        private static void PrintList(List<string> lines)
        {
            Console.OutputEncoding = Encoding.UTF8;
            foreach (var i in lines)
            {
                Console.WriteLine(i);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static List<string> GetLines()
        {
           
            var lines = new List<string>();
            string toppath = "http://www.91x.com/top-91/top-91-1983/";

            HtmlAgilityPack.HtmlWeb web = new HtmlAgilityPack.HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = web.Load(toppath);

            var Headers = doc.DocumentNode.SelectNodes("//div[@id='post-body']/p").ToList();

            foreach (var i in Headers)
            {
                //Console.WriteLine(i.InnerText);
                lines.Add(i.InnerHtml.ToString());
            }

            return lines;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirtlines"></param>
        /// <returns></returns>
        private static List<string> CleanLines( List<string> dirtlines)
        {
            var lines = new List<string>();

            foreach (var i in dirtlines)
            {
                lines.Add(i.Replace("&#8211;", "-").Replace("&#8217;", "'").Replace("ΓÇÖ", "'").Replace("ΓÇô","-").Replace("<br>",""));
            }

            return lines;
        }

        /// <summary>
        /// 
        /// </summary>
        private static List<string> ExtractInfo(List<string> cleanlines)
        {
            var lines = new List<string>();
            string snpattern = @"\d{1,2}\.";
            string apattern = @"\[.][a-zA-Z]+\–";
            foreach (var input in cleanlines)
            {   
                //Song Number.
                MatchCollection snmatches = Regex.Matches(input, snpattern);
                foreach (Match match in snmatches)
                {
                    lines.Add(match.Value.ToString().Replace(".","").Trim());
                }
                //Artist.
                MatchCollection amatches = Regex.Matches(input, apattern);
                foreach (Match amatch in amatches)
                {
                    string t = amatch.Value.ToString();
                    //lines.Add(amatch.Value.ToString());
                }
            }
            return lines;

        }
    }
}